enum LIGHT {
    1,
    0
}

//% color="#05BFE5" iconHeight=40
namespace gamepad {
    //% block="button [BUTTON] is pressed?" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="PIN_Button"
    export function buttonIsPressed(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        Generator.addInit("InitLED","MPythonPin(16,PinMode.OUT).write_digital(0)",true);
        Generator.addCode(`not MPythonPin(${button},PinMode.IN).read_digital()`);
    }

    //% block="LED [LIGHT]" blockType="command"
    //% LIGHT.shadow="dropdown" LIGHT.options="LIGHT" LIGHT.defl="1"
    export function LED(parameter: any, block: any) {
        let statu = parameter.LIGHT.code;
        Generator.addInit("InitLED","MPythonPin(16,PinMode.OUT).write_digital(0)",true);
        Generator.addCode(`MPythonPin(16,PinMode.OUT).write_digital(${statu})`);
    }

    //% block="(NEW) button [BUTTON2] is pressed?" blockType="boolean"
    //% BUTTON2.shadow="dropdown" BUTTON2.options="PIN_Button2"
    export function buttonIsPressed2(parameter: any, block: any) {
        let button = parameter.BUTTON2.code;
        Generator.addCode(`not MPythonPin(${button},PinMode.IN).read_digital()`);
    }

    //% block="(NEW) Joystick [JOYSTICK]" blockType="reporter"
    //% JOYSTICK.shadow="dropdown" JOYSTICK.options="Joystick"
    export function getJoystick(parameter: any, block: any) {
        let stick = parameter.JOYSTICK.code;
        Generator.addCode(`MPythonPin(${stick},PinMode.ANALOG).read_analog()`);
    }
}
