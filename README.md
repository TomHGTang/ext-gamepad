# 游戏手柄扩展板


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接

* 本项目加载链接: ```https://gitee.com/TomHGTang/ext-gamepad```
* 产品介绍: [micro:bit gamepad 遥控手柄](https://www.dfrobot.com.cn/goods-1674.html)

## 描述

适用于 micro:bit 及掌控板的游戏手柄扩展板，有 8 个可编程按钮，板载蜂鸣器、振动电机（掌控板不可用）和 LED，可以控制小车，制作互动游戏等。

本扩展对手柄上的按钮做了映射，可直接使用 <按钮()被按下？> 积木来判断某个按钮是否被按下。支持英文、简体中文、繁体中文三种语言，兼容掌控板（须反插，振动电机不可用）。掌控板支持 MicroPython，与“无线广播”配合使用效果更佳。

因近期游戏手柄改版，增加了摇杆，故新增两个积木块来适配新版手柄。

```注意：新版手柄的板载 LED 与振动马达是绑定在一起的，故无法再单独控制板载 LED。```

## 积木列表

micro:bit|掌控板
---------|--------
![](./arduinoC/_images/blocks1.png)|![](./arduinoC/_images/blocks2.png)

## 示例程序

micro:bit|掌控板
---------|--------
![](./arduinoC/_images/example1.png)|![](./arduinoC/_images/example2.png)

## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
micro:bit        |             |       √       |             |内存过小，不支持 MicroPython
mpython        |             |       √       |       √      | 


## 更新日志

* V0.0.1  基础功能完成
* V0.0.2  支持掌控板
* V0.0.3  掌控板支持 MicroPython，修改按钮描述文字
* V0.0.4  适配新版带摇杆手柄