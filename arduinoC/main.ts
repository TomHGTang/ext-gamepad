enum LIGHT {
    HIGH,
    LOW
}

enum SWITCH {
    HIGH,
    LOW
}

//% color="#05BFE5" iconHeight=40
namespace gamepad {
    //% block="button [BUTTON] is pressed?" blockType="boolean"
    //% BUTTON.shadow="dropdown" BUTTON.options="PIN_Button"
    export function buttonIsPressed(parameter: any, block: any) {
        let button = parameter.BUTTON.code;
        if(Generator.board === 'esp32'){
            Generator.addSetup("InitLED","digitalWrite(P16,LOW);",true);
        }
        Generator.addCode(`!digitalRead(${button})`);
    }

    //% block="LED [LIGHT]" blockType="command"
    //% LIGHT.shadow="dropdown" LIGHT.options="LIGHT" LIGHT.defl="HIGH"
    export function LED(parameter: any, block: any) {
        let statu = parameter.LIGHT.code;
        if(Generator.board === 'microbit'){
            Generator.addCode(`digitalWrite(16,${statu});`);
        }else{
            Generator.addSetup("InitLED","digitalWrite(P16,LOW);",true);
            Generator.addCode(`digitalWrite(P16,${statu});`);
        }
    }

    //% block="Vibrator motor switch [SWITCH]" blockType="command" board="microbit"
    //% SWITCH.shadow="dropdown" SWITCH.options="SWITCH" SWITCH.defl="HIGH"
    export function vibrationMotor(parameter: any, block: any) {
        let statu = parameter.SWITCH.code;
        Generator.addCode(`digitalWrite(12,${statu});`);
    }

    //% block="Vibrator motor intensity [SPEED]" blockType="command" board="microbit"
    //% SPEED.shadow="range"   SPEED.params.min=0    SPEED.params.max=255    SPEED.defl=100
    export function vibrationMotorSpeed(parameter: any, block: any) {
        let speed = parameter.SPEED.code;
        Generator.addCode(`analogWrite(12,${speed}*4);`);
    }

    //% block="(NEW) button [BUTTON2] is pressed?" blockType="boolean"
    //% BUTTON2.shadow="dropdown" BUTTON2.options="PIN_Button2"
    export function buttonIsPressed2(parameter: any, block: any) {
        let button = parameter.BUTTON2.code;
        Generator.addCode(`!digitalRead(${button})`);
    }

    //% block="(NEW) Joystick [JOYSTICK]" blockType="reporter"
    //% JOYSTICK.shadow="dropdown" JOYSTICK.options="Joystick"
    export function getJoystick(parameter: any, block: any) {
        let stick = parameter.JOYSTICK.code;
        Generator.addCode(`analogRead(${stick})`);
    }
}
